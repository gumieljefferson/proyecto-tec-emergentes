import React, { Component } from 'react'
import {Meteor} from 'meteor/meteor'

export default class Signup extends Component {
    constructor(props){
        super(props)
        this.state={
            name:'',
            email:'',
            mobile:'',
            password:'',
            con_password:''
        }
    }

    SubmitForRegister=(e) =>{
        e.preventDefault()
        this.props.history.push('/bienvenido/login')
        const form={
            name:this.state.name,
            email:this.state.email,
            mobile:this.state.mobile,
            password:this.state.password,
            con_password:this.state.con_password
        }
        Meteor.call('newUser',form,function(error,resp){
            if (error){
                alert(error.reason)
            }else{
                alert(resp)
            }
        })
    }
    render() {
        return (
            <div>
                <div>
                    <form className="form-horizontal" onSubmit={this.SubmitForRegister} id="newUsers">
                        <div className="form-rows forms-input">
                            <div className="col-sm-12">
                                <input className="form-control" placeholder="Nombre Completo" type="text" autoComplete="off" onChange={e=>this.setState({name:e.target.value})}/>
                            </div>
                        </div>
                        <br/>
                        <div className="form-rows forms-input">
                            <div className="forms-grpup col-sm-12">
                                <input className="form-control" id="email" placeholder="Correo Electrónico" type="email" autoComplete="off" onChange={e=>this.setState({email:e.target.value})}/>
                            </div>
                        </div><br/>
                        <div className="form-rows forms-input">
                            <div className="forms-grpup col-sm-12">
                                <input className="form-control" id="mobile" placeholder="Teléfono" type="text" autoComplete="off" onChange={e=>this.setState({mobile:e.target.value})}/>
                            </div>
                        </div><br/>
                        <div className="form-rows forms-input">
                            <div className="forms-grpup col-sm-12">
                                <input className="form-control" id="password" placeholder="Contraseña" type="password" onChange={e=>this.setState({password:e.target.value})}/>
                            </div>
                        </div><br/>
                        <div className="form-rows forms-input">
                            <div className="forms-grpup col-sm-12">
                                <input className="form-control" id="con_password" placeholder="Repetir Contraseña" type="password" onChange={e=>this.setState({con_password:e.target.value})}/>
                            </div>
                        </div><br/>
                        <div className="form-rows forms-input">
                            <div className="forms-grpup col-sm-12">
                                <button type="submit" className="btn btn-light btn-radius btn-brd grd1">
                                    Guardar y Continuar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}