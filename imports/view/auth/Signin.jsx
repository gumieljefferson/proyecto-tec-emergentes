import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import {Meteor} from 'meteor/meteor'

export default class Signin extends Component{
    constructor (props){
        super(props)
        this.state={
            email:'',
            password:''
        }
    }
    SubmitForLogin=(e)=>{
        e.preventDefault()
        const mthis=this
        Meteor.loginWithPassword(this.state.email, this.state.password, function(error){
            if(error){
                alert(error.reason )
            }
            else
                //mthis.props.history.push('/dashboard/home')
                window.location.replace('/dashboard/home')
        })
    }
    render() {
        return (
            <div>
                <main>          
                    <section className="login_part section_padding ">
                        <div className="container">
                            <div className="row align-items-center">
                            <div className="col-lg-6 col-md-6">
                                    <div className="login_part_form">
                                        <div className="login_part_form_iner">
                                            <h3>¡Bienvenido de nuevo! <br/>
                                                Por favor Inicie Sesión</h3>
                                            <form className="row contact_form" onSubmit={this.SubmitForLogin}>
                                                <div className="col-md-12 form-group p_star">
                                                    <input type="text" className="form-control" id="email" name="name" autoComplete="off" onChange={e=> this.setState({email:e.target.value})}
                                                        placeholder="Usuario o Email" />
                                                </div>
                                                <div className="col-md-12 form-group p_star">
                                                    <input type="password" className="form-control" id="password" name="password" autoComplete="off"
                                                        onChange={e=> this.setState({password:e.target.value})} placeholder="Contraseña" />
                                                </div>
                                                <div className="col-md-12 form-group">
                                                    <div className="creat_account d-flex align-i tems-center">
                                                        <input type="checkbox" id="f-option" name="selector" />
                                                        <label htmlFor="f-option">Recordar cuenta</label>
                                                    </div>
                                                    <button type="submit" value="submit" className="btn_3">
                                                        INICIAR SESION
                                                    </button>
                                                    <a className="lost_pass" href="#">RECUPERAR CONTRASEÑA</a>
                                                </div>                                                  
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-6 col-md-6">
                                    <div className="login_part_text text-center">
                                        <div className="login_part_text_iner">
                                            <h2>¿Nuevo en la tienda?</h2>
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Rem quas consequatur eaque dignissimos ipsa molestiae, eius optio in quaerat tempore.</p>
                                            <Link to="/bienvenido/registro" className="btn_3"> 
                                                Crear una Cuenta
                                            </Link>
                                        </div>
                                    </div>
                                </div>                              
                            </div>                          
                        </div>
                    </section>
                </main>
            </div>
        )
    }
}
