import React, { Component } from 'react'
import { withTracker } from 'meteor/react-meteor-data'
import { Meteor } from 'meteor/meteor'
import { categoryClass } from '../../models/category/class'

class allCategory extends Component {
    render() {
        const {categorys,subscriptionCategory}=this.props
        return (
            <div>
                <div className="row">
                    <div className="col-12 col-md-12 col-lg-12">
                        <div className="card">
                            <div className="card-header">
                                <h4>Todas las Categorias</h4>
                            </div>
                            <div className="card-body">
                                <div className="table-responsive">
                                    <table className="table table-bordered table-md">
                                        <thead>
                                            <tr>
                                                <th>Nº</th>
                                                <th>Categoria</th>
                                                <th>Descripcion</th>
                                                <th>Estado</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                categorys.map((data,key)=>{
                                                    return <tr key={`pub_${key}`}>
                                                                <td>{key+1}</td>
                                                                <td>{data.name}</td>
                                                                <td>{data.description}</td>
                                                                <td>
                                                                    {data.active?<div className="badge badge-success">Activo</div>:<div className="badge badge-danger">Inactivo</div>}
                                                                </td>
                                                            </tr>
                                                })
                                            }
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                            <div className="card-footer text-right">
                                <nav className="d-inline-block">
                                    <ul className="pagination mb-0">
                                        <li className="page-item disabled">
                                            <a className="page-link" href="#" tabIndex={-1}><i className="fas fa-chevron-left"/></a>
                                        </li>
                                        <li className="page-item active">
                                            <a className="page-link" href="#">1
                                                <span className="sr-only">(current)</span>
                                            </a>
                                        </li>
                                        <li className="page-item">
                                            <a className="page-link" href="#">2</a>
                                        </li>
                                        <li className="page-item">
                                            <a className="page-link" href="#">3</a>
                                        </li>
                                        <li className="page-item">
                                            <a className="page-link" href="#"><i className="fas fa-chevron-right"/></a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withTracker((props)=>{
    const subscriptionCategory= Meteor.subscribe('category',{},'getAllCategory')
    const categorys = categoryClass.find().fetch()
    return {categorys,subscriptionCategory}
})(allCategory)
