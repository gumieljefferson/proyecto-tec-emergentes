import React, { Component } from 'react'
import {categoryClass} from '/imports/models/category/class'
import { withTracker } from 'meteor/react-meteor-data'
import { Meteor } from 'meteor/meteor'

export default class createCategory extends Component {
    constructor(props){
        super(props)
        this.state={
            form:{
                name:null,
                description:null,
            },

            errors:{}
        }
    }
    changeTextInput=(e)=>{
        const value=e.target.value
        const property= e.target.name
        this.setState(prevState=>(
                {form:{
                    ...prevState.form,
                    [property]:value,
                }
            }
        ))
    }
    validateForm=()=>{
        const {form}=this.state
        let errorsform={}
        let formIsValid=true
        if(!form.name){
            formIsValid=false
            errorsform['name']="EL Nombre de Categoria no puede estar vacio"
        }
        const validateItIs = /^[a-zA-Z0-9\u00f1\u00d1\s]+$/g
        if(!validateItIs.test(form.name)){
            formIsValid=false
            errorsform['name']= "El Nombre de Categoria solo debe tener texto"
        }
        
        if(!form.description){
            formIsValid=false
            errorsform['description']="La Descripcion no puede estar vacia"
        }
        this.setState({errors:errorsform})
        return formIsValid
    }
    createNewPublications=(e)=>{
        e.preventDefault()
            const cc = new categoryClass
            const {form}= this.state
        if(this.validateForm()){
            const mthis = this
            cc.callMethod('createNewCategory',form,(error,result)=>{
                if(error){
                    alert(error)
                }else{
                    alert(result)
                    document.getElementById("newCategory").reset()
                    mthis.setState({form:{
                        name:null,
                        description:null,
                    }})
                }
            })
        }else{
            alert('El Formulario tiene errores')
        }
    }
    render() {
        const {errors}= this.state
        return (
            <div>
                <form onSubmit={this.createNewPublications} id="newCategory">
                    <div className="card">
                        <div className="card-header">
                            <h4>Crear categoria</h4>
                        </div>
                        <div className="card-body">
                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <label htmlFor="name">Nombre de Categoria</label>
                                    <input type="text" className={errors.name?"form-control is-invalid":"form-control"} id="name" name={'name'} placeholder="Categoria" autoComplete="off" onChange={this.changeTextInput}/>
                                    {errors.name? <div className="invalid-feedback">{errors.name}</div>:null}
                                </div>
                                <div className="form-group col-md-6">
                                    <label htmlFor="description">Descripción</label>
                                    <input type="text"className={errors.description?"form-control is-invalid":"form-control"} id="description" name={'description'} placeholder="Descripción"  autoComplete="off" onChange={this.changeTextInput}/>
                                    {errors.description? <div className="invalid-feedback">{errors.description}</div>:null}     
                                </div>
                            </div>
                            <div className="card-footer">
                                <button type="submit" className="btn btn-primary">Crear Categoría</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}