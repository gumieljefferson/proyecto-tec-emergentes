import React, { Component } from 'react'
import { publicationsClass } from '../../models/publications/class'
import DatePicker from 'react-datepicker'
import { withTracker } from 'meteor/react-meteor-data'
import { Meteor } from 'meteor/meteor'
import {categoryClass} from '/imports/models/category/class'
import FileUpload from '../../components/FileUpload'
import upload from '../../utils/upload'

class CreatePublication extends Component {
    constructor(props){
        super(props)
        this.state={
            form:{
                title:null,
                description:null,
                price:null,
                starDate:new Date(),
                endDate:new Date(),
                image:null,
                category:null
            },
            errors:{}
        }
    }
    validateForm=()=>{
        const {form}=this.state
        let errorsform={}
        let formIsValid=true
        if(!form.title){
            formIsValid=false
            errorsform.title="El Titulo no puede estar vacio"
        }
        if(!form.description){
            formIsValid=false
            errorsform.description="La Descripcion no puede estar vacia"
        }
        if(!form.price){
            formIsValid=false
            errorsform.price="El Precio no puede estar vacio"
        }
        if(!form.starDate){
            formIsValid=false
            errorsform.starDate="La Fecha Inicial no puede estar vacia"
        }
        if(!form.endDate){
            formIsValid=false
            errorsform.endDate="La Fecha Final no puede estar vacia"
        }
        this.setState({errors:errorsform})
        return formIsValid
    }
    createNewPublications=(e)=>{
        e.preventDefault()
        if(this.validateForm()){
            //alert('Enviando Formulario')
            //console.log(this.state.form)
            const newpublication= new publicationsClass()
            const {form} = this.state
            const {form:{image}}=this.state
            const uf = new upload(image.file,image.self)
            uf.newUpload(
                function(error,success){
                    if(error){
                        console.log('**********')
                        console.log(error)
                        console.log('**********')
                    }else{
                        form.image=success._id
                        newpublication.callMethod('newPublication',form,(error,result)=>{
                            if(error){
                                alert(error)
                            }else{
                                alert(result)
                                document.getElementById("newPublication").reset()
                            }
                        })
                    }
                }
            )
        }else{
           alert('El Formulario tiene errores')
        }
    }
    changeTextInput=(e)=>{
        const value=e.target.value
        const property= e.target.name
        this.setState(prevState=>(
                {form:{
                    ...prevState.form,
                    [property]:value,
                }
            }
        ))
    }
    changeDateInput=(type,date)=>{
        this.setState(prevState=>(
            {form:{
                ...prevState.form,
                [type]:date,
            }
        }
    ))
    }
    changeSelectInput=(e)=>{
        const value=e.target.value
        this.setState(prevState=>(
            {
                form:{
                    ...prevState.form,
                    category:value,
                }
            }
        ))
    }
    changeFileInput=(data)=>{
        const inputFile=data.file
        if(inputFile && inputFile[0]){
            let reader = new FileReader()
            reader.onload = function(v){
                $('#previewimage').attr('src',v.target.result)
            }
            reader.readAsDataURL(inputFile[0])
            this.setState(prevState=>(
                {
                    form:{
                        ...prevState.form,
                        image:data,
                    }
                }
            ))
        }
    }
    render() {
        const {errors}= this.state
        const {categorys,subscriptionCategory}= this.props
        return (
            <div>
                <section className="section">
                    <div className="section-body">
                        <div className="row">
                            <div className="col-12 col-md-6 col-lg-12">
                                <div className="card">
                                    <div className="card-header">
                                        <h4>Crear Nueva Publicacion</h4>
                                    </div>
                                    {!subscriptionCategory.ready()?
                                        <h1>Cargando...</h1>
                                        :
                                        <div className="card-body">
                                        <form onSubmit={this.createNewPublications} id="newPublication"> 
                                            <div className="row">
                                                <div className="col-md-6">
                                                    <div className="form-group">
                                                        <label>Titulo de Publicacion</label>
                                                        <input type="text" className={errors.title?"form-control is-invalid":"form-control"} name={'title'} autoComplete="off" onChange={this.changeTextInput}/>
                                                            {errors.title? <div className="invalid-feedback">{errors.title}</div>:null}                         
                                                    </div>
                                                    <div className="form-group">
                                                        <label>Descripción</label>
                                                        <textarea type="text" className={errors.description?"form-control is-invalid":"form-control invoice-input"}  name='description' autoComplete="off" onChange={this.changeTextInput}></textarea>
                                                        {errors.description? <div className="invalid-feedback">{errors.description}</div>:null} 
                                                    </div>
                                                    <div className="form-group">
                                                        <label>Seleccione una Categoria</label>
                                                        <select className="form-control" onChange={this.changeSelectInput}>
                                                            <option defaultValue >Seleccione Categoria</option>
                                                            {
                                                                categorys.map((category,key)=>{
                                                                    return <option  key={"category" + key}value={category._id}>{category.name}</option>
                                                                })
                                                            }
                                                        </select>
                                                    </div>
                                                    <div className="form-group">
                                                        <label>Archivo</label>
                                                        <FileUpload changeFileInput={this.changeFileInput}/>
                                                    </div>
                                                    <div className="form-group">
                                                        <label>Precio</label>
                                                        <div className="input-group">
                                                            <div className="input-group-prepend">
                                                                <div className="input-group-text">
                                                                    Bs.
                                                                </div>
                                                            </div>
                                                            <input type="text" className={errors.price?"form-control is-invalid":"form-control currency"} name='price' autoComplete="off" onChange={this.changeTextInput}/>                                                  
                                                            {errors.price? <div className="invalid-feedback">{errors.price}</div>:null}
                                                        </div>
                                                    </div>
                                                    <div className="form-group">
                                                        <label>Fecha de Publicacion</label><div>
                                                            <DatePicker selected={this.state.form.starDate} className={errors.starDate?"form-control is-invalid":"form-control datemask"} name='starDate' autoComplete="off"
                                                                onChange = {  date   =>{
                                                                    this.changeDateInput('starDate',date)
                                                                }}/>
                                                            {errors.starDate? <div className="invalid-feedback" style={{display:'block'}}>{errors.starDate}</div>:null} 
                                                        </div>
                                                    </div>
                                                    <div className="form-group">
                                                        <label>Fecha de Culminacion</label><div>
                                                            <DatePicker selected={this.state.form.endDate} className={errors.starDate?"form-control is-invalid":"form-control datemask"} name='endDate' autoComplete="off"
                                                                onChange = { date =>{ 
                                                                    this.changeDateInput('endDate',date)
                                                                }}/>
                                                            {errors.endDate? <div className="invalid-feedback" style={{display:'block'}}>{errors.endDate}</div>:null} 
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-md-6 d-flex justify-content-center">
                                                    <div className="author-box-center">
                                                        <img alt="image" src="/dashboard/img/users/user-1.png"  id="previewimage"className="author-box-picture" style={{width: '100%', height: '500px', borderRadius: '5%'}}/>
                                                        <div className="clearfix" />
                                                        <div className="author-box-job d-flex justify-content-center">Vista Previa</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="submit" className="btn btn-primary">Guardar</button>
                                        </form>               
                                    </div>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

export default withTracker((props)=>{
    const subscriptionCategory= Meteor.subscribe('category',{},'getCategory')
    const categorys = categoryClass.find().fetch()
    return {categorys,subscriptionCategory}
})(CreatePublication)