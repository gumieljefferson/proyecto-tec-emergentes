import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import {Meteor} from 'meteor/meteor'
import { withTracker } from 'meteor/react-meteor-data'
import { publicationsClass } from '../../models/publications/class'
import { categoryClass } from '../../models/category/class'

const categorySelect= new ReactiveVar(undefined)

class shop extends Component {
    selectCategory=(category)=>{
        categorySelect.set(category?category:undefined)
    }
    render() {
        const {categorys, categorySubscription, publications, subscriptionPublications}=this.props
        return (
            <div>              
                <main>
                    <div className="slider-area ">
                        <div className="single-slider slider-height2 d-flex align-items-center">
                        <div className="container">
                            <div className="row">
                            <div className="col-xl-12">
                                <div className="hero-cap text-center">
                                <h2>Tienda</h2>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <section className="popular-items latest-padding">
                        <div className="container">
                        <div className="row product-btn justify-content-between mb-40">
                            <div className="properties__button">
                                <nav>                                           
                                    <div className="nav nav-tabs" id="nav-tab" role="tablist" >   
                                            {
                                                categorys.map((data,key)=>{
                                                    return <div key={`pub_${key}`} >
                                                                <a className="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true" onClick={()=>this.selectCategory(data)} >{data.name}</a>
                                                            </div>
                                                })
                                            } 
                                        </div>
                                </nav>
                            </div>
                            <div className="grid-list-view"></div>
                        </div>
                        <div className="tab-content" id="nav-tabContent">
                            <div className="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                            <div className="row">
                                        {
                                            publications.map((data,key)=>{
                                                return <div key={`pub_${key}`} className="col-xl-4 col-lg-4 col-md-6 col-sm-6" >
                                                            <div className="single-popular-items mb-50 text-center">
                                                            <div className="popular-img">
                                                            <img src={data.urlFile}/>
                                                            <div className="img-cap">
                                                                <span>{data.price}</span>
                                                            </div>
                                                            <div className="favorit-items">
                                                                <span className="flaticon-heart" />
                                                            </div>
                                                            </div>
                                                            <div className="popular-caption">
                                                            <h3><a href="product_details.html">{data.title}</a></h3>
                                                            </div>
                                                            </div>
                                                        </div>
                                            })
                                        }
                            </div>
                        </div>
                        </div>
                        </div>
                    </section>
                    <div className="shop-method-area">
                        <div className="container">
                        <div className="method-wrapper">
                            <div className="row d-flex justify-content-between">
                            <div className="col-xl-4 col-lg-4 col-md-6">
                                <div className="single-method mb-40">
                                <i className="ti-package" />
                                <h6>Free Shipping Method</h6>
                                <p>aorem ixpsacdolor sit ameasecur adipisicing elitsf edasd.</p>
                                </div>
                            </div>
                            <div className="col-xl-4 col-lg-4 col-md-6">
                                <div className="single-method mb-40">
                                <i className="ti-unlock" />
                                <h6>Secure Payment System</h6>
                                <p>aorem ixpsacdolor sit ameasecur adipisicing elitsf edasd.</p>
                                </div>
                            </div> 
                            <div className="col-xl-4 col-lg-4 col-md-6">
                                <div className="single-method mb-40">
                                <i className="ti-reload" />
                                <h6>Secure Payment System</h6>
                                <p>aorem ixpsacdolor sit ameasecur adipisicing elitsf edasd.</p>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </main>
            </div>
        )
    }
}
export default withTracker((props)=>{
    const optionspublications={
        category : categorySelect.get()
    }
    const subscriptionPublications = Meteor.subscribe('publications',optionspublications,'getAllPublications')
    const subscriptionCategory= Meteor.subscribe('category',{},'getCategory')
    const categorys = categoryClass.find().fetch()
    const publications= publicationsClass.find({},{sort:{createdAt:-1}})
    return {categorys,subscriptionCategory,subscriptionPublications,publications}
})(shop)
