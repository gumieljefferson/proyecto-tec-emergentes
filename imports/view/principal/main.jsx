import React, { Component } from 'react'
import {Link, Switch} from 'react-router-dom'
import SwitchRoutes from '/imports/routes/SwitchRoutes'
import { publicationsClass } from '../../models/publications/class'
import { categoryClass } from '../../models/category/class'
import { withTracker } from 'meteor/react-meteor-data'

class Main extends Component {
    render() {
        const {publications,subscriptionPublications}=this.props
        return (
            <div>
                <div className="slider-area ">
                    <div className="slider-active">
                        <div className="single-slider slider-height d-flex align-items-center slide-bg">
                            <div className="container">
                                <div className="row justify-content-between align-items-center">
                                    <div className="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                                        <div className="hero__caption">
                                            <h1 data-animation="fadeInLeft" data-delay=".4s" data-duration="2000ms">¡Bienvenido! a nuestra Reposteria</h1>
                                        </div>
                                    </div>
                                    <div className="col-xl-3 col-lg-3 col-md-4 col-sm-4 d-none d-sm-block">
                                        <div className="hero__img" data-animation="bounceIn" data-delay=".4s">
                                            <img src="/principal/img/watch.png"  className=" heartbeat" style={{width:'150%', height: '150%'}}/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <section className="new-product-area section-padding30">
                    <div className="container">
                        <div className="row">
                            <div className="col-xl-12">
                                <div className="section-tittle mb-70">
                                    <h2>Nuestros Productos</h2>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            {
                                publications.map((data,key)=>{
                                    return <div key={"public_"+key} className="col-xl-4 col-lg-4 col-md-6 col-sm-6" >
                                                <div className="single-new-pro mb-30 text-center">
                                                    <div className="product-img" style={{width:'100%', height: '100%'}} >
                                                        <img src={data.urlFile}/>
                                                    </div>
                                                    <div className="product-caption">
                                                        <h3>{data.title}</h3>
                                                    </div>
                                                </div>
                                            </div>
                                })
                            }
                        </div>
                    </div>
                </section>
                <div className="popular-items section-padding30">
                    <div className="container">

                        {/*<div className="row justify-content-center">
                                <div className="col-xl-7 col-lg-8 col-md-10">
                                    <div className="section-tittle mb-70 text-center">
                                        <h2>Productos Populares</h2> 
                                    </div>
                                </div>
                                </div>
                                <div className="row">
                                {
                                    publications.map((data,key)=>{
                                        <div key={"public"+key} className="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                                            <div className="single-popular-items mb-50 text-center">
                                                <div className="popular-img">
                                                    <img src="/principal/img/popular1.png" alt=""/>
                                                    <div className="img-cap">
                                                        <span>{data.price}</span>
                                                    </div>
                                                    <div className="favorit-items">
                                                        <span className="flaticon-heart"></span>
                                                    </div>
                                                </div>
                                                <div className="popular-caption">
                                                    <h3>{data.title}</h3>
                                                    <h5>{data.description}</h5>
                                                </div>
                                            </div>
                                        </div>
                                    })
                                }
                                </div>
                                <div className="row justify-content-center">
                                <div className="room-btn pt-70">
                                    <Link to="/bienvenido/tienda" className="btn view-btn1">Ver más Productos</Link>
                            </div>
                            </div>*/}
                            <div className="shop-method-area">
                        <div className="container">
                        <div className="method-wrapper">
                            <div className="row d-flex justify-content-between">
                            <div className="col-xl-4 col-lg-4 col-md-6">
                                <div className="single-method mb-40">
                                <i className="ti-package" />
                                <h6>Free Shipping Method</h6>
                                <p>aorem ixpsacdolor sit ameasecur adipisicing elitsf edasd.</p>
                                </div>
                            </div>
                            <div className="col-xl-4 col-lg-4 col-md-6">
                                <div className="single-method mb-40">
                                <i className="ti-unlock" />
                                <h6>Secure Payment System</h6>
                                <p>aorem ixpsacdolor sit ameasecur adipisicing elitsf edasd.</p>
                                </div>
                            </div> 
                            <div className="col-xl-4 col-lg-4 col-md-6">
                                <div className="single-method mb-40">
                                <i className="ti-reload" />
                                <h6>Secure Payment System</h6>
                                <p>aorem ixpsacdolor sit ameasecur adipisicing elitsf edasd.</p>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withTracker((props)=>{
    const subscriptionPublications = Meteor.subscribe('publications', {}, 'getAllPublications')
    const publications = publicationsClass.find().fetch()
    return { publications,subscriptionPublications}
})(Main)