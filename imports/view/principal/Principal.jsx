import React, { Component } from 'react'
import {Link, Switch} from 'react-router-dom'
import SwitchRoutes from '/imports/routes/SwitchRoutes'

export default class Principal extends Component{
    constructor(props){
        super(props)
        this.state={
            loader:true
        }
    }
    componentDidMount(){
        import '/imports/assets/principal/css/'
        import '/imports/assets/principal/js/'
        import desingPrincipal from '/imports/assets/principal/js/main'
        const mthis=this
        setTimeout(function(){
            desingPrincipal()
            mthis.setState({loader:false})
            $('#preloader-active').delay(450).fadeOut('slow');
        },2000)
    }
    render() {
        const {routes}= this.props
            return (
                <div>
                    {this.state.loader?
                    <div id="preloader-active">
                        <div className="preloader d-flex align-items-center justify-content-center">
                            <div className="preloader-inner position-relative">
                                <div className="preloader-circle"></div>
                                <div className="preloader-img pere-text">
                                    <img src="/principal/img/Charge.gif" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>
                    :
                    <div>
                        <header>
                            <div className="header-area">
                                <div className="main-header header-sticky">
                                    <div className="container-fluid">
                                        <div className="menu-wrapper">

                                            <div className="logo">
                                                <a ><img src="/principal/img/logo.webp" style={{width:'40%', height: '40%'}} /></a>
                                            </div>

                                            <div className="main-menu d-none d-lg-block">
                                                <nav>
                                                    <ul id="navigation">
                                                        <li><Link to="/bienvenido/principal">Inicio</Link></li>
                                                        <li><Link to="/bienvenido/tienda">Tienda</Link></li>
                                                        <li>
                                                            <Link to="/bienvenido/login">Registro</Link>
                                                        </li>
                                                    </ul>
                                                </nav>
                                            </div>

                                            <div className="header-right">
                                                <ul>
                                                    <li>
                                                        <div className="nav-search search-switch">
                                                            <span className="flaticon-search"></span>
                                                        </div>
                                                    </li>
                                                    <li> <a href="login.html"><span className="flaticon-user"></span></a></li>
                                                    <li><a href="cart.html"><span className="flaticon-shopping-cart"></span></a> </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div className="col-12">
                                            <div className="mobile_menu d-block d-lg-none"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </header>
                        <main>
                            <Switch>
                                {
                                    routes.map((route,i)=>{
                                return <SwitchRoutes key={i} {...route} />
                                })
                                }
                            </Switch>
                                
                        </main>
                        <footer>

                            <div className="footer-area footer-padding">
                                <div className="container">
                                    <div className="row d-flex justify-content-between">
                                        <div className="col-xl-3 col-lg-3 col-md-5 col-sm-6">
                                            <div className="single-footer-caption mb-50">
                                                <div className="single-footer-caption mb-30">

                                                    <div className="footer-logo">
                                                        <a href="index.html"><img src="/principal/img/logo2_footer.png" alt="" /></a>
                                                    </div>
                                                    <div className="footer-tittle">
                                                        <div className="footer-pera">
                                                            <p>Asorem ipsum adipolor sdit amet, consectetur adipisicing elitcf sed do
                                                                eiusmod tem.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-xl-2 col-lg-3 col-md-3 col-sm-5">
                                            <div className="single-footer-caption mb-50">
                                                <div className="footer-tittle">
                                                    <h4>Enlaces rápidos</h4>
                                                    <ul>
                                                        <li><a href="#">Acerca de </a></li>
                                                        <li><a href="#"> Ofertas y Descuentos</a></li>
                                                        <li><a href="#"> Conseguir un cupón</a></li>
                                                        <li><a href="#"> Contáctenos</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-xl-3 col-lg-3 col-md-4 col-sm-7">
                                            <div className="single-footer-caption mb-50">
                                                <div className="footer-tittle">
                                                    <h4>Contáctenos</h4>
                                                    <ul>
                                                        <li><a href="#">Direccion: Calle Uno Nº123</a></li>
                                                        <li><a href="#">Celular: 12345678</a></li>
                                                        <li><a href="#">Email: tortas@reposteria.com</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-xl-3 col-lg-3 col-md-5 col-sm-7">
                                            <div className="single-footer-caption mb-50">
                                                <div className="footer-tittle">
                                                    <h4>Soporte</h4>
                                                    <ul>
                                                        <li><a href="#">Preguntas Frecuentes</a></li>
                                                        <li><a href="#">Terminos y Condiciones</a></li>
                                                        <li><a href="#">Politica de Privacidad</a></li>
                                                        <li><a href="#">Reportar un problema</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </footer>
                        

                    </div>
                    }
                </div>
            )
    }   
}