import { Meteor } from 'meteor/meteor';
import Files from '../models/files/class'

export default class uploadFiles{//clase en javascript
    constructor(file,self){//el archivo  y el componente y su contexto
        this.file = file,//archivo
        this.self = self//contexto
    }
    newUpload = (callback)=>{//metodo que recibe como parametro una funcion callback nos ayuda a mandar informacion o datos a otro archivo mediante una funcion
        const self = this.self //tranferir dato a una variable self
        const files = this.file //tranferir dato a una variable file

        if(files && files[0]){//verificando si existe un archivo
            var file = files[0];
            if(file){
                let uploadInstance = Files.insert({
                    file:file,
                    meta:{
                        locator:self.props.fileLocator,
                        userId:Meteor.userId()
                    },
                    chunkSize:'dynamic',
                    allowWebWorkers:true,
                },false)
                self.setState({
                    uploading:uploadInstance,
                    inProgress:true
                })

                uploadInstance.on('start',function(){
                    console.log('Iniciando subida de archivo')
                })

                uploadInstance.on('uploaded',function(error,fileObj){
                        console.log('subiendo: ',fileObj)                    
                        self.refs['filesinput'].value = ''
                        self.setState({
                            uploading:[],
                            progress:0,
                            inProgress:false
                        })
                })

                uploadInstance.on('error',function(error,fileObj){
                    console.log('error al subir el archivo '+error)
                    callback(error,null)
                })

                uploadInstance.on('progress',function(progress,fileObj){
                    console.log('porcentaje de subida '+ progress)
                    self.setState({
                        progress:progress
                    })
                })

                uploadInstance.on('end',function(error,fileObj){
                    callback(null,fileObj)
                })

                uploadInstance.start()
            }
        }
    }
}
