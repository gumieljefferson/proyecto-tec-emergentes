import { data } from 'jquery';
import React, { Component } from 'react'

export default class Nav extends Component{
    logoutSystem=()=>{
        const {history} = this.props
        Meteor.logout(function() {
            Meteor._localStorage.removeItem('Meteor.loginToken');
            Meteor._localStorage.removeItem('Meteor.loginTokenExpires');
            Meteor._localStorage.removeItem('Meteor.userId');
        });
    }
    render() {
        const {link}=this.props
        return (
            <div>
                <nav className="navbar navbar-expand-lg main-navbar sticky">
                    <div className="form-inline mr-auto">
                        <ul className="navbar-nav mr-3">
                            <li><a href="#" data-toggle="sidebar" className="nav-link nav-link-lg
                                                                        collapse-btn"> <i data-feather="align-justify" /></a></li>
                            <li><a href="#" className="nav-link nav-link-lg fullscreen-btn">
                                    <i data-feather="maximize" />
                                </a></li>
                            <li>
                                <form className="form-inline mr-auto">
                                    <div className="search-element">
                                        <input className="form-control" type="search" placeholder="Search" aria-label="Search"
                                            data-width={200} />
                                        <button className="btn" type="submit">
                                            <i className="fas fa-search" />
                                        </button>
                                    </div>
                                </form>
                            </li>
                        </ul>
                    </div>
                    <ul className="navbar-nav navbar-right"><li className="dropdown"><a href="#" data-toggle="dropdown"
                                className="nav-link dropdown-toggle nav-link-lg nav-link-user"> <img alt="image"
                                    src="/dashboard/img/user.jpg" className="user-img-radious-style" /> <span
                                    className="d-sm-none d-lg-inline-block" /></a>
                            <div className="dropdown-menu dropdown-menu-right pullDown">
                                <div className="dropdown-title">{data.username}</div>
                                <div className="dropdown-divider" />
                                <button onClick={this.logoutSystem} className="dropdown-item has-icon text-danger"> <i
                                        className="fas fa-sign-out-alt" />
                                    Cerrar Sesion
                                </button>
                            </div>
                        </li>
                    </ul>
                </nav>

            </div>
        )
    }
}