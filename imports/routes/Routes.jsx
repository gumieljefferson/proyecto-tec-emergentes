import React, {Component} from 'react'
import Signin from '/imports/view/auth/Signin'
import Signup from '/imports/view/auth/Signup'
import Layout from '/imports/view/layout/Layout'
import Principal from '/imports/view/principal/Principal'
import main from '/imports/view/principal/main'
import shop from '/imports/view/shop/shop'
import {Home} from '/imports/view/home/Home'
import {useLocation} from 'react-router-dom';
import categoryroutes from './categoryroutes'
import publicationRoutes from './publicationRoutes'
import {createBrowserHistory} from "history";
import {Roles} from 'meteor/alanning:roles'
const history = createBrowserHistory();

function NoMatch(){
    let location = useLocation();
    return (
        <div>
            <h3>
                La pagina solicitada no existe <code> {location.pathname}</code>              
            </h3>
        </div>
    );
}

export const routes= [
    {
        path:'/bienvenido',
        component:Principal,
        authenticated:false,
        routes:[
            {
                path:'/bienvenido/principal',
                component:main
            },
            {
                path:'/bienvenido/login',
                component:Signin
            },
            {
                path:'/bienvenido/registro',
                component:Signup
            },
            {
                path:'/bienvenido/tienda',
                component:shop
            },
        ]
    },
    {
        path:'/dashboard',
        component:Layout,
        authenticated:true,
        routes:[
            {
                path:"/dashboard/home",
                component:Home
            },
            ...publicationRoutes,
            ...categoryroutes,
        ]
    },
    {
        path:'/otrasrutas',
        component:Layout,
        authenticated:true
    },
    {
        path:'*',
        component: NoMatch 
    }
]

function getAllRoutesAuthenticated(routes){
    let routesAuthenticated=[]
    routes.forEach((value,index) => {
        if(JSON.stringify(value.authenticated)===JSON.stringify(true)){
            routesAuthenticated.push(value.path)
            if(value.routes){
                value.routes.forEach((v,i ) => {
                    routesAuthenticated.push(v.path)
                });
            }           
        }
    });
    return routesAuthenticated
}

export const checkAuthUser=function(authenticated){
    const path= history.location.pathname
    const isAuthenticatedPage = getAllRoutesAuthenticated(routes).includes(path)
    if(authenticated && isAuthenticatedPage){
        history.replace('/dashboard/home')
    }else if(!authenticated&& isAuthenticatedPage){
        history.replace('/')
        location.reload()
    }
}