import createCategory from "../view/category/createCategory"
import allCategory from "../view/category/allCategory"

export default routes= [
    {
        path:'/dashboard/create-category',
        component:createCategory,
        permission:"createCategory"
    },
    {
        path:'/dashboard/all-categorys',
        component:allCategory,
        permission:"viewallCategory"
    },
]