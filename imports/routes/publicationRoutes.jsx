import CreatePublication from '/imports/view/publications/CreatePublication'
import userPublications from '/imports/view/publications/userPublications'
import editPublications from '../view/publications/editPublications'

export default routes= [
    {
        path: "/dashboard/create-publications",
        component: CreatePublication,
        permission:"createpublication"
    },
    {
        path: "/dashboard/edit-publications",
        component: editPublications,
        permission:"editpublication"
    },
    {
        path: "/dashboard/user-publications",
        component: userPublications,
        permission:"allpublicationuser"
    },

]