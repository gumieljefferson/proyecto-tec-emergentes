import React from 'react'
import {Redirect, Route} from 'react-router-dom'
import {Roles} from 'meteor/alanning:roles'

const SwitchRoutes = (route) => {
    const auth= !!Meteor.userId()
    return (
        <Route
            path={route.path}
            render={(props)=>{
                if(route.authenticated){
                    if(auth){
                        return <route.component {...props} routes={route.routes}/>
                    }else{
                        return <Redirect to='/bienvenido/login'/>
                    }

                }else{
                    if(auth&& route.path== '/bienvenido/login')
                        return window.location.replace('/dashboard/home')
                    else
                        return <route.component {...props} routes={route.routes}/>
                }
                   // return (<route.component {...props} routes={route.routes}/>)
            }}
        />
    )
}
 export default SwitchRoutes