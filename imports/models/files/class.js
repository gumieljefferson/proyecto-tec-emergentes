import {Meteor} from 'meteor/meteor';
import {FilesCollection} from 'meteor/ostrio:files';

export default Files = new FilesCollection({
    storagePath: '/SIS414/files',
    downloadRoute:'/files/SIS414',
    collectionName: 'Files',
    permissions: 0o775,
    allowClientCode: false,
    cacheControl:'public, max-age=31536000',
    onbeforeuploadMessage(){
        return 'La carga del archivo esta en progreso. la carga sera cancelada si sale de la pagina'
    },
    onBeforeUpload(file){
        if(file.size <= 10485760 && /|png|jpg|jpeg/i.test(file.extension)){
            return true;
        }
        return 'Por favor suba una imagen con peso de menos de 10mb'
    },
    downloadCallback(fileObj){
        if(this.params.query.download=='true'){
            Files.update(fileObj._id,{$inc: {'meta.dowload':1}});
        }
        return true;
    }
});