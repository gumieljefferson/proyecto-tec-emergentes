import { Meteor} from "meteor/meteor";
import Files from '/imports/models/files/class'

Meteor.methods({
    RemoveFile(id){
        Files.remove({_id:id}, function(error){
            if(error){
                console.log('No se pudo remover el archivo '+ error.reason)
            }else{
                console.log('Eliminado con exito')
            }
        })
    }
})

Meteor.publish('files.all', function(){
    return Files.find().cursor
} )