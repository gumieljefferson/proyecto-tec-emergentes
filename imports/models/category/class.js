import { Class } from 'meteor/jagi:astronomy';
const categoryCollection = new Mongo.Collection('category',{idGeneration:'MONGO'});

export const categoryClass = Class.create({
    name: 'categoryClass',
    collection: categoryCollection,
    fields: {
        name:{//de ser obligatorio le estoy diciendo que el nombre tiene que tener minimo 3 letras
            type:String,
            validators: [{
              type: 'regexp',
              param:  /^[a-zA-Z0-9\s\u00f1\u00d1]{6,}$/g,//solo pueden ingresar letras y numero (ñ)  y que sean mayor o igual a 6
              resolveError({ name, param }) {
                return `El nombre de la categoria debe contener solo letras y ser mayor o igual a 6 letras`;
              }
            }]
        },
        description: {
            type: String,
        },
        active:{
            type: Boolean,
            default: true
        }
    }
});

if (Meteor.isServer) {
    categoryClass.extend({
        fields:{
            user:String,
            
        },
        behaviors:{
            timestamp:{
                hasCreatedField: true,
                createdFieldName: 'createdAt',
                hasUpdatedField: true,
                updatedFielName: 'updatedAt'
            }
        }
    })
}