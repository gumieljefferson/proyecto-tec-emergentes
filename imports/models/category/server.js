import {categoryClass} from './class'
import { queryCategory } from './query';

categoryClass.extend({
    meteorMethods:{
        createNewCategory(forms){
            this.set(forms)
            this.user = Meteor.userId()
            this.save()
            return 'Creado exitosamente'
        }
    }
});

Meteor.publish('category',function(options,type){
    try {
        const subs = new queryCategory(options,this)
        return subs[type]()
    }catch(error){
        this.stop()
        throw new Meteor.Error(403,error.reason)
    }
})