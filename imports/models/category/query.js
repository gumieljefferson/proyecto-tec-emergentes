import { categoryClass } from "../category/class"

export const queryCategory = class{
    constructor(options, mthis){
        this.options=options
        this.mthis= this
    }
    getCategory(){
        try{
            return categoryClass.find({active:true},{sort:{createdAt:-1}})
        } catch (error){
            console.log(error)
        }
    }
    getAllCategory(){
        try {
            return categoryClass.find({},{sort:{createdAt:-1}})
        }catch(error){
            console.log(error)
        }
    }
}