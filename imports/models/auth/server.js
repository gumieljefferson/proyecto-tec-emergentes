Meteor.methods({
    
    newUser:function(form){
        if(form.password != form.con_password)
            throw new Meteor.Error(406, "Las contraeñas deben ser iguales"  )
        Accounts.createUser({
            createAdd:new Date(),
            username:form.name,
            email:form.email,
            mobile:form.mobile,
            password:form.password,
            con_password:form.con_password,
            profile:{
                username:form.name,
                email:form.email,
                mobile:form.mobile,
                password:form.password,
            }
        })
        return 'Ususario creado. Por favor Inicie Sesión'
    }
})

Meteor.methods({
    createRoles(){
        //ESTO SE PUEDE HACER DE FORMA DINAMICA DEPENDIENDO EL DESARROLLO DE SU SISTEMA
        Meteor.roles.insert({
            _id:'createpublication', 
            description:'Crear Publicacion', 
            groupdescription:'publicaciones', 
            group:'people',
            children:[],
            key:new Meteor.Collection.ObjectID()
        })
        Meteor.roles.insert({_id:'allpublicationuser',description:'Ver mis Publicacion',groupdescription:'publicaciones',
            group:'people',
            children:[],
            key:new Meteor.Collection.ObjectID()
        })
        Meteor.roles.insert({_id:'editpublication',description:'Editar Publicacion',groupdescription:'publicaciones',group:'people',children:[],key:new Meteor.Collection.ObjectID()})
        Meteor.roles.insert({_id:'downpublication',description:'Eliminar Publicacion',groupdescription:'publicaciones',group:'people',children:[],key:new Meteor.Collection.ObjectID()})
        Meteor.roles.insert({_id:'createCategory',description:'Crear Categoria',groupdescription:'categorias',group:'admin',children:[],key:new Meteor.Collection.ObjectID()})
        Meteor.roles.insert({_id:'viewallCategory',description:'Ver Categorias',groupdescription:'categorias',group:'admin',children:[],key:new Meteor.Collection.ObjectID()})

       //Roles.setUserRoles(Meteor.userId(),["createpublication","allpublicationuser","editpublication","downpublication","createCategory","viewallCategory"])
        return 'creado correctamente'
    }
})


Meteor.publish(null, function () {
    if (this.userId) {
      return Meteor.roleAssignment.find({ 'user._id': this.userId });
    } else {
      this.ready()
    }
})
