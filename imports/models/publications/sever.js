import { publicationsClass } from "./class";
import Files from '/imports/models/files/class'
import {categoryClass} from '/imports/models/category/class'
import {queryPublications} from '/imports/models/publications/query'

publicationsClass.extend({
    meteorMethods:{
        newPublication(form){
            try{
                //console.log(form)
                const URL =Files.findOne({_id:form.image}).link()
                var newURL = URL.replace(/^.*\/\/[^\/]+/,'')
                const category = categoryClass.findOne({_id:new Meteor.Collection.ObjectID(form.category)})
                this.title=form.title
                this.description=form.description
                this.price=parseInt(form.price)
                this.nameCategory=category.name
                this.idCategory= category._id
                this.idFile=form.image
                this.urlFile=newURL
                this.starDate=form.starDate
                this.endDate=form.endDate
                this.user=Meteor.userId()
                this.username=Meteor.user().username
                this.create_view= new Date()
                this.save()
                return ("Guardado con Exito")
            }catch(error){
                console.log(error)
                throw new Meteor.Error(403,error.reason)
            }
        },
        editPublication(form){
            try{
                if (form.image){
                    const URL =Files.findOne({_id:form.image}).link()
                    var newURL = URL.replace(/^.*\/\/[^\/]+/,'')
                    const category = categoryClass.findOne({_id:new Meteor.Collection.ObjectID(form.category)})
                    this.title=form.title
                    this.description=form.description
                    this.price=parseInt(form.price)
                    this.nameCategory=category.name
                    this.idCategory= category._id
                    this.idFile=form.image
                    this.urlFile=newURL
                    this.starDate=form.starDate
                    this.endDate=form.endDate
                    this.user=Meteor.userId()
                    this.username=Meteor.user().username
                    this.create_view= new Date()
                    this.save()
                    return ("Editado con Nueva Imagen")
                }else{
                    const category = categoryClass.findOne({_id:new Meteor.Collection.ObjectID(form.category)})
                    this.title=form.title
                    this.description=form.description
                    this.price=parseInt(form.price)
                    this.nameCategory=category.name
                    this.idCategory= category._id
                    this.starDate=form.starDate
                    this.endDate=form.endDate
                    this.user=Meteor.userId()
                    this.username=Meteor.user().username
                    this.create_view= new Date()
                    this.save()
                    return ("Editado sin Nueva Imagen")
                }
                
            }catch(error){
                console.log(error)
                throw new Meteor.Error(403,error.reason)
            }
        },
        updateStatePublication(){
            try{
                this.active=this.active?false:true
                return this.save()
            }catch(error){
                console.log(error)
                throw new Meteor.Error(403,error.reason)
            }
        }
    }
})

Meteor.publish('publications',function(options,type){
    try {
        const subs = new queryPublications(options,this)
        return subs[type]()
    }catch(error){
        this.stop()
        throw new Meteor.Error(403,error.reason)
    }
})