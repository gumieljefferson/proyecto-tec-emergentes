import { Class } from 'meteor/jagi:astronomy';
const publicationsCollection = new Mongo.Collection('publications',{idGeneration:'MONGO'});

export const publicationsClass = Class.create({
    name: 'publicationsClass',
    collection: publicationsCollection,
    fields: {
        title: {
            type: String,
            
        },
        description: {
            type: String,
            
        },
        price: {
            type: Number,
            
        },
        starDate: {
            type: Date,
            
        },
        endDate: {
            type: Date,
        },
        idCategory:{
            type:Mongo.ObjectID,
        },
        nameCategory:{
            type: String,
        },
        idFile:{
            type:String,
        },
        urlFile:{
            type:String,
        },
        active:{
            type:Boolean,
            default:true,
        },
        create_view:{
            type:Date,
        },
        username:String,
    }
});

if (Meteor.isServer) {
    publicationsClass.extend({
      fields: {
        user: String,
      },
      behaviors: {
        timestamp: {
          hasCreatedField: true,
          createdFieldName: 'createdAt',
          hasUpdatedField: true,
          updatedFieldName: 'updatedAt'
        }
      }
    });
}
